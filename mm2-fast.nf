#!/usr/bin/env nextflow

process mm2_fast_index {
// Creates a mm2_fast index
//
// input:
//   path fa - Reference FASTA
//   val params - Additional Parameters
//
// output:
//   tuple => emit: idx_files
//     path(fa) - Reference FASTA
//     path("${fa}.*") - Index Files

// require:
//   params.mm2_fast$dna_ref
//   params.mm2_fast$mm2_fast_index_parameters

  storeDir "${params.shared_dir}/${fa}/mm2_fast_index"
  tag "${fa}"
  label 'mm2_fast_container'
  label 'mm2_fast_index'
  cache 'lenient'

  input:
  path fa
  val parstr

  output:
  tuple path(fa), path("${fa}.*"), emit: idx_files

  script:
  """
#  mm2_fast index ${parstr} ${fa}
  """
}


process mm2_fast {
// Runs mm2_fast
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   REF
//   params.mm2_fast$mm2_fast_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'mm2_fast_container'
  label 'mm2_fast'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  path fa
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.paf'), optional: true, emit: pafs
  tuple val(pat_name), val(run), val(dataset), path('*.sam'), optional: true, emit: sams

  script:
  """
  mm2-fast ${parstr} ${fa} ${fq} -t ${task.cpus} > ${dataset}-${pat_name}-${run}.sam
  """
}


process mm2_fast_samtools_sort {
// Runs mm2_fast
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   REF
//   params.mm2_fast$mm2_fast_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'mm2_fast_samtools_container'
  label 'mm2_fast'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  path fa
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.sorted.bam'), optional: true, emit: bams

  script:
  """
  mm2-fast ${parstr} ${fa} ${fq} -t ${task.cpus - 1} -R "@RG\\tID:${dataset}-${pat_name}-${run}\\tSM:${dataset}-${pat_name}-${run}\\tLB:NULL\\tPL:NULL" |\
  samtools sort -@ 1 -o ${dataset}-${pat_name}-${run}.sorted.bam -
  """
}
